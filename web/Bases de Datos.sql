DROP TABLE Usuario;
DROP TABLE Noticia;
DROP TABLE Comentario;
DROP TABLE Rol;

        

CREATE TABLE Usuario (
 nickname VARCHAR(100) NOT NULL,
 nombre VARCHAR(100) NOT NULL,
 apellido VARCHAR(100) NOT NULL,
 contrasena VARCHAR(100) NOT NULL,
 imagen VARCHAR(100),
 correo VARCHAR(100),
 PRIMARY KEY (nickname)
);

CREATE TABLE Rol (
 nickname VARCHAR(100) NOT NULL,
 rol VARCHAR(100) NOT NULL,
 PRIMARY KEY (nickname,rol) 
);

CREATE TABLE Noticia (
 identificador INT NOT NULL,
 nickname VARCHAR(100) NOT NULL,
 titulo VARCHAR(100) NOT NULL,
 urlImg VARCHAR(256) NOT NULL,
 encabezado VARCHAR(256) NOT NULL,
 seccion VARCHAR(100) NOT NULL,
 subseccion VARCHAR(100) NOT NULL,
 esPrincipal CHAR(1) DEFAULT 'F' NOT NULL,
 cuerpo VARCHAR(5000) NOT NULL,
 fecha DATE,
 likes INT,
 dislikes INT,
 PRIMARY KEY (identificador,nickname),
 FOREIGN KEY (nickname) REFERENCES Usuario (nickname)
);

CREATE TABLE Comentario (
 identificadorNot INT NOT NULL,
 nicknameAutor VARCHAR(100) NOT NULL,
 nicknameUser VARCHAR(100) NOT NULL,
 Identificador CHAR(10) NOT NULL,
 FOREIGN KEY (identificadorNot,nicknameAutor) REFERENCES Noticia (identificador,nickname),
 FOREIGN KEY (nicknameUser) REFERENCES Usuario (nickname),
 PRIMARY KEY (identificadorNot,nicknameAutor,nicknameUser,Identificador),
 Comentario VARCHAR(200)
);

CREATE TABLE Pilotof1(
piloto VARCHAR(100) NOT NULL,
escuderia VARCHAR(100) NOT NULL,
puntuacion INT NOT NULL,
PRIMARY KEY (piloto)
);



INSERT INTO Usuario (nickname,nombre,apellido,contrasena,imagen)
VALUES('antpesq','Antonio','Pesquera','817948044',NULL);

INSERT INTO Usuario (nickname,nombre,apellido,contrasena,imagen)
VALUES('davmarc','David','Marciel','65805908',NULL);

INSERT INTO Usuario (nickname,nombre,apellido,contrasena,imagen)
VALUES('frareoy','Francisco Javier','Reoyo','220318999',NULL);

INSERT INTO Usuario (nickname,nombre,apellido,contrasena,imagen)
VALUES('marcart','Mario','Carton','74113764',NULL);


INSERT INTO Rol (nickname, rol)
VALUES('antpesq','writer');

INSERT INTO Rol (nickname, rol)
VALUES('davmarc','writer');

INSERT INTO Rol (nickname, rol)
VALUES('frareoy','writer');

INSERT INTO Rol (nickname, rol)
VALUES('marcart','writer');



INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(1,
    'antpesq',
    'Daniel Ricciardo recibe el Laureus como deportista revelación del año', 
    'uploads/thumb1.jpg', 
    'El piloto de Fórmula 1 Daniel Ricciardo ha sido distinguido como revelación del año en los 
    Premios Laureus. El australiano ha sido el unico deportista entre los participantes del 
    Gran Circo.',
    'F1', 
    'noticias',
    'T',
    '<p class= "cuerpo">Ricciardo ha superado en las votaciones a los otros nominados, el futbolista colombiano James Rodríguez, el alemán Mario Götze, el tenista Marin Cilic y la esquiadora Mikaela Shiffrin. Además, ha sido el único piloto relacionado con F1 que ha sido premiado, ya que ni Lewis Hamilton ni Mercedes han recibido el galardón ni como mejor deportista de la pasada temporada ni como mejor equipo.</p>

<p class = "cuerpo">El piloto australiano no ha podido acudir a recoger el premio en Shanghái, pues ya se encuentra en Bahréin preparando el cuarto Gran Premio de la temporada.</p>

<p class = "cuerpo">Escrito por Adrián Mancebo para Autobild.es</p>',
    '2015-04-30');

INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(2,
    'davmarc',
    'Sordo tendrá nuevo Hyundai... en 2016', 
    'uploads/thumb2.jpg', 
    ' Un problema con el calendario de homologación - relacionado con la producción de la versión 
    de calle de tres puertas - retrasará el programa a Montecarlo 2016. Se introducirón mejoras en 
    el actual i20 WRC para buscar algún podio en 2015.', 
    'WRC', 
    'noticias',
    'F',
    '<p class = "cuerpo">Al final no será en verano, sino en 2016 cuando Hyundai tendrá listo el nuevo I20. La firma coreana tenía la esperanza de introducir el coche en esta temporada pero Dani Sordo tendrá que esperar a la próxima temporada.</p>

<p class = "cuerpo">La empresa asegura que se ha producido un retraso en la homologación prevista del nuevo coche causado por el programa de producción de la versión de calle del tres puertas, lo que significa que el equipo tiene como objetivo marcado debutar con el nuevo i20 WRC en el Campeonato del Mundo de Rallyes de la FIA 2016 en el Rallye de Montecarlo.</p>

<p class = "cuerpo">Mientras, Hyundai ha llevado a cabo cuatro días de test en Almería con el nuevo i20 WRC. Los test marcaron la siguiente fase importante del desarrollo del coche, que hará su debut en el Campeonato Mundial de Rally de la FIA 2016 (WRC).</p>

<p class = "cuerpo">El piloto de pruebas del equipo Kevin Abbring y su copiloto Sebastián Marshall estaban al volante del i20 WRC de nueva generación junto con Thierry Neuville y Nicolas Gilsoul. El trabajo se centró en el desarrollo del motor y chasis, así como pruebas de altitud con la última versión del coche, que se ha mejorado en comparación con la versión utilizada en pruebas anteriores.</p>

<p class = "cuerpo">Refinar el coche. El retraso da tiempo extra al equipo para desarrollar y refinar el coche de cinco puertas antes de su debut competitivo, mientras que al mismo tiempo se evolucionan las especificaciones del actual Hyundai i20 WRC cara a las pruebas del WRC de esta temporada.</p>

<p class = "cuerpo">Se van a introducir una serie de mejoras en el actual Hyundai i20 WRC cara a los rallyes que quedan de 2015 en un intento por ayudar a la lucha del equipo por conseguir subir al podio en las pruebas que restan en sólo su segunda temporada en el Campeonato del Mundo de Rallyes.</p>

<p class = "cuerpo">"Teníamos la esperanza de presentar el nuevo coche en la segunda mitad de esta temporada, pero por desgracia, un problema con el calendario de homologación - relacionado con la producción de la versión de calle de tres puertas - retrasará nuestro programa. Por ello, hemos optado por seguir centrándonos en la versión de cinco puertas del i20 WRC de nueva generación que competirá en el Rally de Monte-Carlo de 2016. Este calendario añadido nos da tiempo extra para asegurarnos de que el coche tendrá el rendimiento que buscamos cuando se inicie la temporada. Al mismo tiempo, el actual Hyundai i20 WRC se someterá a una serie de cambios importantes que esperamos nos permita luchar por el podio en esta temporada", asegura el director del equipo Michel Nandan comentó.</p>

<p class = "cuerpo">Escrito por Miguel Sanz para Marca</p>',
    '2015-04-10');

INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(3,
    'frareoy',
    'Márquez: "Las Ducati están dando mucha guerra', 
    'uploads/thumb3.jpg', 
    '"Valentino Rossi está muy rápido, y Lorenzo volverá a estar en cabeza, luchando por las 
    victorias", comentó el catalán en su blog con Repsol Honda.', 
    'Motos', 
    'noticias',
    'F',
    '<p class = "cuerpo">Marc Márquez (Repsol Honda) ha realizado balance de las dos primeras carreras del Mundial de MotoGP y ha subrayado que "las Ducati están dando mucha guerra" y que Valentino Rossi (Yamaha) "está muy rápido", añadiendo que "Lorenzo también volverá a estar delante, peleando por victorias".</p>

<p class = "cuerpo">"Ahora no podemos desconectar absolutamente ni un minuto porque este fin de semana tenemos otro gran premio que será igual de exigente o más. Ya hemos visto que las Ducati están dando mucha guerra, que Rossi está muy rápido, y no dudo que Lorenzo también volverá a estar delante, peleando por victorias. Así que tenemos que estar concentrados al cien por cien", analizó Márquez en su blog con Repsol Honda.</p>

<p class = "cuerpo">Después de no lograr el resultado que le "hubiera gustado" en la primera carrera del año en Catar, el vigente bicampeón sabía que en Austin iba "a poder luchar por la victoria", como finalmente ocurrió pese a que "la pista estaba algo resbaladiza".</p>

<p class = "cuerpo">"Pude ponerme en cabeza pronto, marcar un buen ritmo y escaparme. Afortunadamente tuvimos buen tiempo, no como el primer día de gran premio. Este tipo de carreras pueden parecer más sencillas, pero no lo son. Quizá sí son algo más aburridas, incluso para el piloto, pero precisamente por eso saber mantener la concentración es clave. Supimos hacerlo, ¡y hemos logrado la primera victoria del año!", se alegró.</p>

<p class = "cuerpo">En el Gran Premio de Las Américas, el catalán logró "una de las poles más especiales" de toda su carrera. "Fue un momento de pura adrenalina, no tengo otra manera de describirlo. Cuando paré la moto miré el tiempo que quedaba e intenté hacer un cálculo mental lo más rápido posible. Vi que tenía tiempo para intentarlo, muy justo, pero lo tenía. Por eso no quise perder ni un segundo y eché a correr a por la segunda moto. Con la intensidad del instante, no puedo negar que ese fue el momento más bonito de todo el fin de semana, casi más que la propia carrera", aseguró.</p>

<p class = "cuerpo">Por último, Márquez se despidió del recientemente fallecido Joan Moreta, quien fue presidente de la RFME y vicepresidente de la FIM. "No quiero olvidarme de una de las personas que más nos ha ayudado a Álex (en referencia a su hermano) y a mí en nuestra carrera, Joan Moreta. Nos deja un buen amigo, una persona que lo dio todo por nuestro deporte. Descanse en paz", concluyó.</p>',
    '2015-04-14');

INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(4,
    'marcart',
    'Sainz: "Ya he competido de noche en GP3 en Abu Dabi"', 
    'uploads/thumb4.jpg', 
    'Carlos Sainz, piloto de Toro Rosso, afronta el GP de Bahréin con la incertidumbre de no haber
     competido nunca allá, pero con la experiencia de haber pilotado ya de noche en Abu Dabi.', 
    'F1', 
    'noticias',
    'F',
    '<p class = "cuerpo">Carlos Sainz, piloto de Toro Rosso, afronta el GP de Bahréin con la incertidumbre de no haber competido nunca allí, pero con la experiencia de haber pilotado ya de noche en Abu Dabi.</p>

<p class = "cuerpo">"Este circuito suele deparar una gran carrera y el año pasado recuerdo que fue uno de los grandes momentos del campeonato. Correr de noche en un Fórmula 1 será un reto interesante, algo diferente, aunque ya he competido de noche en GP3 en Abu Dabi", recuerda en un comunicado del equipo.</p>

<p class = "cuerpo">Sainz describió el circuito de Sakhir como un trazado "de largas rectas que ofrece muchas oportunidades para adelantar, y un sector medio más apretado con algunas curvas rápidas, seguido de un tramo exigente para los frenos en la curva 10".</p>

<p class = "cuerpo">El piloto español, decimocuarto el domingo pasado en el GP de China, visita por primera vez Baréin: "Nunca he estado allí, así que no sé qué esperar del país", comentó.</p>',
    '2015-04-14');

INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(5,
    'antpesq',
    'El antebrazo baja a Pedrosa de la Honda sin fecha de regreso', 
    'uploads/thumb8.jpg', 
    'El piloto español deja de competir hasta que solucione los problemas en su brazo derecho:
     "No sé cómo va a ser, pero no puedo seguir así".', 
    'Motos', 
    'noticias',
    'T',
    '<p class = "cuerpo">Lo peor de la carrera de Qatar para Dani Pedrosa no ha sido su flojo sexto puesto sino las razones que le han llevado a no ser competitivo en toda la carrera. El catalán ha sufrido de nuevo dolores en el antebrazo derecho, del que ya se operó el año pasado tras la carrera de Jerez, y ha comunicado muy apesadumbrado que en estas condiciones no puede pilotar, dando a entender que se baja de su Honda sin fecha de regreso. Su declaración, en la que no ha aceptado preguntas, ha sonado a despedida, pero tanto él, como Honda y todo el paddock espera que arregle esos problemas para volver a subirse a la moto, porque ahora mismo no está garantizada su presencia en la parrilla de Austin dentro de dos semanas.</p>

<p class = "cuerpo">Así se ha expresado el catalán: “Básicamente, quiero explicar mi situación. Ahora hace un año justo que vengo teniendo muchísimos problemas con el antebrazo derecho, los cuales estaba sufriendo bastante en silencio en esta difícil situación. He intentado por todos los medios arreglarlo, el año pasado, como sabéis, ya tuve una operación a mitad de temporada, la cual no me ayudó en ningún sentido y la situación durante el año pasado fue muy difícil. Con muchos problemas al acabar el año, estuve viajando por todo el planeta, probando con diferentes doctores para encontrar una solución para mi brazo y casi ninguno me recomendó poder operar. Me recomendaron no operar y mejorar el brazo con algunas técnicas, pero es evidente que no puedo seguir corriendo de esta manera ya que no puedo dar lo máximo de mí y hace ya tiempo que sufro esta situación y es muy difícil seguir compitiendo así, de esta manera. El equipo sabía de esta situación, evidentemente, y ahora hemos llegado a un punto en el que tengo que retomar el camino de intentar solucionar el problema. No tengo muchas respuestas de cómo va ser, pero tengo que encontrar la manera de arreglarlo ya que es evidente que así no puedo seguir corriendo, porque no puedo dar lo mejor de mí”.</p>

<p class = "cuerpo">Y terminó con agradecimientos: “Quiero agradecer el apoyo que siempre he tenido de Honda, Repsol, Arai, Red Bull, Alpinestar, Pars Europ, Custos, por apoyarme y seguir en estos duros momentos, pero hay que tirar para adelante, intentaremos sacar lo máximo que pueda y dar las gracias a mis fans. A ver si podemos encontrar una solución pronto, pero por ahora no tengo respuestas de qué es lo que va a haber”.</p>

<p class = "cuerpo">Escrito por Mela Chércoles para Marca.</p>',
    '2015-03-29');

INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(6,
    'davmarc',
    'Fernando Alonso se gana a McLaren', 
    'uploads/thumb5.jpg', 
    'Los técnicos rompieron a aplaudir a Fernando tras la calificación.Fernando ha creado un buen 
    ambiente muy rápido, asegura Boullier a MARCA sobre el oscuro y malhumorado ogro que ve Lauda.', 
    'F1', 
    'noticias',
    'T',
    '<p class = "cuerpo">A pesar del ritmo del actual ritmo del MP4-30 y de que le es imposible por el momento superar a otros coches que no sean los Manor, Fernando Alonso está teniendo una buena adaptación en su regreso a McLaren. No se trata ya de la relación con Eric Boullier, director deportivo, que es excelente y con el que se le ve en actitud confidente a menudo, sino con sus técnicos cercanos, el grupo fuerte que se encarga de su coche. "El oscuro y malhumorado piloto que envenenó Ferrari", como ha proclamado Niki Lauda estos días, no se aprecia luego en los boxes de los equipos por donde pasa.</p>

<p class = "cuerpo">A Fernando siempre le ha gustado crear un vínculo especial con sus ingenieros más próximos y con estos suelen conectar a la perfección. Su primer técnico de pista en Renault, hoy con un alto cargo en el organigrama de Red Bull, Paul Monagham, sigue siendo un amigo personal y queda con él a menudo fuera del circuito. Tanto ocurre con gente de Ferrari, como Maximo Rivola, mánager de Ferrari, al que conoció en sus días de Minardi. O con Andrea Stella, que no ha dudado en seguirle a McLaren después de toda su vida en Maranello.</p>

<p class = "cuerpo">No es cierto que el ambiente en torno a él sea irrespirable y eso lo saben muy bien sus compañeros en Ferrari, con los que sigue quedando a montar en bicicleta o a cenar a menudo. Otra cosa es que a los altos responsables de la marca les cante las cuarenta cuando cree que las cosas no van bien y siempre con el objetivo de la mejora a la vista. Esa implicación que un Kimi, por ejemplo, no ha tenido ni un solo día de su vida en el trabajo.</p>

<p class = "cuerpo">Volviendo a McLaren, Fernando ya encuentra parroquia. El sábado en Shanghái, tras la calificación en la que dio las dos primeras vueltas del día en la sesión definitiva, por la avería de la mañana, el asturiano quedó muy sorprendido. Al entrar a la reunión técnica, sus ingenieros rompieron espontáneamente en un aplauso. El piloto no sabía muy bien a qué se debía la celebración o si era una broma. Le sacaron de dudas al decirle que nadie se creía que, sin un solo reglaje y a ciegas, hubiera podido quedarse a milésimas de Jenson Button.</p>

<p class = "cuerpo">Con cierta vergüenza, Fernando luego pensaba la diferencia con el año pasado en Ferrari, donde pese a conseguir dos podios y a batir continuamente a otro campeón del mundo como Raikkonen, de forma abrumadora y el ambiente era fúnebre y con exigencias absurdas por parte de Marco Mattiacci, que nunca le reconoció nada.</p>

<p class = "cuerpo">Boullier comentaba ayer a MARCA: "Es verdad que siempre hace falta un tiempo para encajar las piezas y crear buen ambiente, pero me sorprende que en el caso de Fernando sea tan rápido. En ese sentido es muy positivo para el trabajo que tenemos por delante", dice el galo. El ogro del que se dice que es imposible trabajar a su lado no es tal. Incluso ayer ante de la carrera, Fernando volvía a coincidir con un hombre de Ferrari y bromeaba con él a carcajadas. Ese es todo el mal ambiente del que se habla.</p>

<p class = "cuerpo">Escrito por Marco Canseco para Marca</p>',
    '2015-04-14');

INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(7,
    'frareoy',
    'Rossi: "Espero que este contrato no sea el último"', 
    'uploads/thumb6.jpg', 
    'El piloto de 36 años ha expresado con más claridad que nunca que quiere seguir en 2017, cuando
     expira su actual contrato con Yamaha.', 
    'Motos', 
    'noticias',
    'F',
    '<p class = "cuerpo">El italiano, de 36 años y líder del Mundial de MotoGP, ha expresado con más claridad que nunca que quiere seguir en 2017, cuando expira su actual contrato con Yamaha.</p>

<p class = "cuerpo">"Tengo un contrato de dos años, pero espero que no sea el último", señaló en una entrevista de televisión.</p>',
    '2015-04-15');

INSERT INTO Noticia (identificador, nickname, titulo,urlImg,encabezado,seccion,subseccion,
esPrincipal,cuerpo,fecha)
VALUES(8,
    'marcart',
    'Hamilton: "Soy más fuerte mentalmente que Rosberg"', 
    'uploads/thumb7.jpg', 
    '"Siempre he dicho que él es muy fuerte mentalmente, pero creo que en esta temporada yo lo soy
     un poco más", declaró el piloto británico.', 
    'F1', 
    'noticias',
    'F',
    '<p class = "cuerpo">El piloto británico Lewis Hamilton (Mercedes), actual campeón del Mundial de Fórmula Uno, ha afirmado que es "mentalmente más fuerte" que su compañero, el alemán Nico Rosberg, en unas declaraciones que recoge hoy la cadena BBC. "Siempre he dicho que él es muy fuerte mentalmente, pero creo que este año yo lo soy un poco más", declaró Hamilton, quien fue acusado tras el Gran Premio de China del pasado fin de semana por su compañero de comprometer innecesariamente su carrera.</p>

<p class = "cuerpo">Rosberg, que finalizó en segunda posición en Shanghái, por detrás del piloto británico, aseguró que Hamilton redujo deliberadamente su ritmo para hacerlo más vulnerable ante un ataque de Sebastian Vettel (Ferrari), quien terminó en el tercer puesto.</p>

<p class = "cuerpo">El actual campeón del mundo, sin embargo, negó que esa fuera su estrategia y que "solo quería controlar su ritmo para minimizar el desgaste de los neumáticos".</p>

<p class = "cuerpo">"Mi trabajo no es cuidar la carrera de Nico sino administrar el coche y llevarlo de vuelta a casa de una pieza y lo más rápido posible. No frené intencionadamente a ninguno de los coches. Me estaba centrando en mí mismo. Si Nico me quisiera haber pasado, lo hubiera intentado, pero no lo hizo", dijo Hamilton tras la carrera. El director de la escudería Mercedes, el austríaco Toto Wolff, salió en defensa de sus dos pilotos y dijo que la actitud de ambos "es entendible".</p>

<p class = "cuerpo">"Nico estaba preocupado; no podía llegar al ritmo de Lewis porque tenía que proteger sus neumáticos mientras Sebastian incrementaba su velocidad. Entiendo la actitud de ambos", explicó Wolff. "No creo que Lewis lo hiciera a propósito, es algo de lo que ya hemos hablado", declaró el jefe de Mercedes, quien añadió que el equipo es capaz de sobrellevar la rivalidad entre sus dos pilotos sin necesidad de "impartir órdenes de equipo".</p>

<p class = "cuerpo">"Si alguna vez tuviéramos que interferir entre los dos porque corremos el riesgo de perder una carrera, entonces lo haríamos", afirmó Wolff. "Lo que entendemos como una orden clara de equipo, como las que se han visto en el pasado, en el sentido de no autorizar un adelantamiento, por ejemplo, es algo que no sucederá", subrayó el austríaco.</p>',
    '2015-04-14');


INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Lewis Hamilton','Mercedes',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Sebastian Vettel','Ferrari',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Nico Rosberg','Mercedes',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Felipe Massa','Williams',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Kimi Räikkönen','Ferrari',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Valtteri Bottas','Williams',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Felipe Nasr','Sauber',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Daniel Ricciardo','Red Bull',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Romain Grosjean','Lotus',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Nico Hulkenberg','Force India',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Max Verstappen','Toro Rosso',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Carlos Sainz','Toro Rosso',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Marcus Ericsson','Sauber',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Daniil Kvyat','Red Bull',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Sergio Pérez','Force India',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Jenson Button','McLaren',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Fernando Alonso','McLaren',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Will Stevens','Manor Marussia',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Roberto Mehri','Manor Marussia',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Pastor Maldonado','Lotus',0);
INSERT INTO Pilotof1(piloto,escuderia,puntuacion)VALUES('Kevin Magnussen','McLaren',0);