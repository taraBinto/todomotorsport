<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Pilotof1DTO"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TodoMotorsport</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="style.css" />

    </head>
    <body>


        <div id="wrapper" >
            <div id="header">

                  <div id="sesion">
                    <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Iniciar Sesion</a> 
                        <a href="registro.html" style="padding-left: 2%; padding-right: 2%">Registrarse</a>
                        <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Mostrar Perfil</a>
                        <a href="subida.html" style="padding-left: 2%; padding-right: 2%">Subir Noticia</a>
                        <a href="subir-clasificacion.html" style="padding-left: 2%; padding-right: 2%">Subir Puntuacion F1</a>
                        <a href="logOut" style="padding-left: 2%">Cerrar Sesion</a>
                </div>
                
                 <div class="logo">
                    <a href="index.html"><img src="images/banner.jpg" alt="TodoMotorsport"/></a>
                    <a href="index.html"><h1>Todo Motorsport</h1></a>
                    <a href="index.html"><p>Tu p�gina sobre el mundo de la competici�n de Motor</p></a>
                </div>

                <div class="break"></div>

                <ul>
                    <li><a style="background-color: #DC1000" href="f1.html">F�rmula 1</a></li>
                    <li><a href="motociclismo.html">Motociclismo</a></li>
                    <li><a href="wrc.html">WRC</a></li>
                    <li><a href="wtcc.html">WTCC</a></li>
                </ul>

                <div class="break"></div>

                <div class="subseccion">
                    <ul>
                        <li><a>Calendario</a></li>
                        <li><a style="background-color: #a9a9a9" href="getClasificacion">Clasificaci�n</a></li>
                        <li><a href = "circuitos.html">Circuitos</a></li>
                        <li><a href="pilotos.html">Pilotos</a></li>
                        <li><a>Escuderias</a></li>
                    </ul>
                </div>

                <div class="break"></div>

            </div>


            <%  ArrayList<Pilotof1DTO> pilotos = (ArrayList<Pilotof1DTO>) request.getAttribute("pilotos");
            %>


            <div id="content">

                <div id="clasificacion">
                    <table id="tablaClasificacion">
                        <caption><h1 style="padding: 3px;">Clasificacion de Formula 1</h1></caption>
                        <thead><tr><th>Posici�n</th><th>Piloto</th><th>Escuder�a</th><th>Puntos</th></tr></thead>

                        <%
                            if (pilotos != null) {
                                for (int i = 0; i < pilotos.size(); i++) {
                                    Pilotof1DTO piloto = pilotos.get(i);
                        %>
                        <tr>
                            <td><%=(i + 1)%>�</td>
                            <td><%=piloto.getNombre()%></td>
                            <td><%=piloto.getEscuderia()%></td>
                            <td><%=piloto.getPuntuacion()%></td></tr>

                        <%      }
                            }%>
                    </table> 
                </div>

                <div id="footer">
                    <p>Copyright &copy; 2015 - <a href="index.html">TodoMotorsport</a> &middot; Todos los Derechos Reservados</p>
                </div>

            </div>
        </div>
    </body>
</html>
