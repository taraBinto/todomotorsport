<%-- 
    Document   : muestraRegistro
    Created on : 21-may-2015, 19:00:17
    Author     : david
--%>

<%@page import="DAO.UsuarioDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>Usuario registrado</title>        
    </head>
</body>


<div id="wrapper">

    <div id="header">

        <div id="sesion">
                    <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Iniciar Sesion</a> 
                        <a href="registro.html" style="padding-left: 2%; padding-right: 2%">Registrarse</a>
                        <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Mostrar Perfil</a>
                        <a href="subida.html" style="padding-left: 2%; padding-right: 2%">Subir Noticia</a>
                        <a href="subir-clasificacion.html" style="padding-left: 2%; padding-right: 2%">Subir Puntuacion F1</a>
                        <a href="logOut" style="padding-left: 2%">Cerrar Sesion</a>
                </div>

        <div class="logo">
            <a href="index.html"><img src="images/banner.jpg" alt="TodoMotorsport"/></a>
            <a href="index.html"><h1>Todo Motorsport</h1></a>
            <a href="index.html"><p>Tu página sobre el mundo de la competición de Motor</p></a>
        </div>

        <div class="break"></div>

        <hr class="linea">

        <div class="break"></div>

    </div>
    <center>
        <% UsuarioDTO usuario = (UsuarioDTO) request.getAttribute("usuario");%>
        <h1>Añadido usuario: <br></h1>
        <img class="none" src="<%= usuario.getRutaImagen()%>"> <br>
        <h1>
            nombre: <%= usuario.getNombre()%> <br>
            apellido: <%= usuario.getApellido()%> <br>
            nick: <%= usuario.getNickName()%> <br>
            email: <%= usuario.getCorreo()%>
        </h1>
    </center>
    <div id="footer">
        <p>Copyright &copy; 2015 - <a href="index.html">TodoMotorsport</a> &middot; Todos los Derechos Reservados</p>
    </div>

</div>

</html>

