<%@page import="DAO.NoticiaDTO"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TodoMotorsport</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="style.css" />

    </head>
    <body>


        <% NoticiaDTO noticia = (NoticiaDTO) request.getAttribute("noticia");%>

        <div id="wrapper">

            <div id="header">
               
                  <div id="sesion">
                    <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Iniciar Sesion</a> 
                        <a href="registro.html" style="padding-left: 2%; padding-right: 2%">Registrarse</a>
                        <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Mostrar Perfil</a>
                        <a href="subida.html" style="padding-left: 2%; padding-right: 2%">Subir Noticia</a>
                        <a href="subir-clasificacion.html" style="padding-left: 2%; padding-right: 2%">Subir Puntuacion F1</a>
                        <a href="logOut" style="padding-left: 2%">Cerrar Sesion</a>
                </div>

                 <div class="logo">
                    <a href="index.html"><img src="images/banner.jpg" alt="TodoMotorsport"/></a>
                    <a href="index.html"><h1>Todo Motorsport</h1></a>
                    <a href="index.html"><p>Tu p�gina sobre el mundo de la competici�n de Motor</p></a>
                </div>

                <div class="break"></div>

                <ul>
                    <li><a href="f1.html">F�rmula 1</a></li>
                    <li><a href="motociclismo.html">Motociclismo</a></li>
                    <li><a href="wrc.html">WRC</a></li>
                    <li><a href="wtcc.html">WTCC</a></li>
                </ul>

                <div class="break"></div>

                <div class="subseccion">
                    <ul>
                        <li><a>Calendario</a></li>
                        <li><a href="getClasificacion">Clasificaci�n</a></li>
                        <li><a href ="circuitos.html">Circuitos</a></li>
                        <li><a href="pilotos.html">Pilotos</a></li>
                        <li><a>Escuderias</a></li>
                    </ul>
                </div>

                <div class="break"></div>

            </div>


            <div id="content">

                <div class="post2">
                    <div class="thumb"><a><img src="<%= noticia.getRutaImagen()%>" alt="" /></a></div>
                    <h2><a><%= noticia.getTitulo()%></a></h2>      
                    <p class="date">Publicado el <%=noticia.getFecha()%> por <%=noticia.getNickName()%></p>

                    <div  class = "noticia">
                        <p class = "encabezado">
                            <%=noticia.getEncabezado()%>
                        </p>

                        <%= noticia.getCuerpo()%>
                    </div>


                </div>  

            </div>


            <div id="footer">
                <p>Copyright &copy; 2015 - <a href="index.html">TodoMotorsport</a> &middot; Todos los Derechos Reservados</p>
            </div>

        </div>

    </body>
</html>
