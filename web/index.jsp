<%@page import="java.util.ArrayList"%>
<%@page import="DAO.NoticiaDTO"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TodoMotorsport</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="style.css" />

    </head>
    <body>


        <div id="wrapper">

            <div id="header">

                <div id="sesion">
                    <% if (session.getAttribute("usuario") == null) { %>
                    <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Iniciar Sesion</a> 
                    <a href="registro.html" style="padding-left: 2%; padding-right: 2%">Registrarse</a>
                    <%} else {%>
                    <a href="servletPerfil" style="padding-left: 2%; padding-right: 2%">Mostrar Perfil</a>
                    <a href="subida.html" style="padding-left: 2%; padding-right: 2%">Subir Noticia</a>
                    <a href="subir-clasificacion.html" style="padding-left: 2%; padding-right: 2%">Subir Puntuacion F1</a>
                    <a href="logOut" style="padding-left: 2%">Cerrar Sesion</a>
                    <%}%>
                </div>

                <div class="logo">
                    <a href="index.html"><img src="images/banner.jpg" alt="TodoMotorsport"/></a>
                    <a href="index.html"><h1>Todo Motorsport</h1></a>
                    <a href="index.html"><p>Tu p�gina sobre el mundo de la competici�n de Motor</p></a>
                    <form action="index.html" method="post">                 
                        <input type="text" name="busqueda" />
                        <input type="submit" value="Buscar" />
                    </form>
                </div>

                <div class="break"></div>

                <ul>
                    <li><a href="f1.html">F�rmula 1</a></li>
                    <li><a href="motociclismo.html">Motociclismo</a></li>
                    <li><a href="wrc.html">WRC</a></li>
                    <li><a href="wtcc.html">WTCC</a></li>
                </ul>
                <div class="break"></div>
            </div>

            <% ArrayList<NoticiaDTO> noticias = (ArrayList<NoticiaDTO>) request.getAttribute("noticias");%>
            <%-- = "Hay noticias: " + noticias.size() --%>

            <div id="content">
                <%

                    NoticiaDTO noticia;

                    if (noticias != null && noticias.size() != 0) {
                        for (int i = 0; i < noticias.size(); i++) {
                            noticia = noticias.get(i);
                %>


                <div class="post">
                    <div class="thumb">
                        <a href = "servletNoticias?noticia=<%= noticia.getIdentificador()%>">
                            <img src="<%= noticia.getRutaImagen()%>" alt="" />
                        </a>
                    </div>
                    <h2>
                        <a href = "servletNoticias?noticia=<%= noticia.getIdentificador()%>">
                            <%= noticia.getTitulo()%>
                        </a>
                    </h2>
                    <p class="date">Publicado el <%=noticia.getFecha()%> por <%=noticia.getNickName()%></p>
                    <p>
                        <%=noticia.getEncabezado()%>
                    </p>

                    <a href = "servletNoticias?noticia=<%= noticia.getIdentificador()%>" class="continue">
                        Leer M�s
                    </a>
                </div>

                <%
                    }
                } else {
                %>

                <br/>
                <center><h1> No hay noticias disponibles </h1></center>
                <br/>

                <%}%>
            </div>

            <div id="footer">
                <p>Copyright &copy; 2015 - <a href="index.html">TodoMotorsport</a> &middot; Todos los Derechos Reservados</p>
            </div>

        </div>
    </body>
</html>
