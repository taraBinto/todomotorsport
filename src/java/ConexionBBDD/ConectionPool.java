/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionBBDD;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.InitialContext;

/**
 *
 * @author david
 */
public class ConectionPool {

    private static ConectionPool pool = null;
    private static DataSource dataSource = null;

    private ConectionPool() {
        try {
            InitialContext ic = new InitialContext();
            dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/todoMotorSport");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
