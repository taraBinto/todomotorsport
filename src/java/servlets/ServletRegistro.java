/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import DAO.UsuarioDAO;
import DAO.UsuarioDTO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author david
 */
@MultipartConfig
@WebServlet(name = "servletRegistro", urlPatterns = {"/servletRegistro"})
public class ServletRegistro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String nombre = (String) request.getParameter("nombre");
        String clave = (String) request.getParameter("clave");
        String apellido = (String) request.getParameter("apellido");
        String nick = (String) request.getParameter("nick");
        String email = (String) request.getParameter("email");

        String imagen = cargaFoto(request);

        boolean aceptaAcuerdo;
        if (request.getParameter("aceptaAcuerdo") != null) {
            aceptaAcuerdo = true;
        } else {
            aceptaAcuerdo = false;
        }

        if (aceptaAcuerdo) {
            //codigo que guarda la noticia en la bbdd
            try {
                UsuarioDAO nDAO = new UsuarioDAO();
                nDAO.guardaUsuario(nick, clave, nombre, apellido, email, imagen, "writer");

                UsuarioDTO usuario = nDAO.getUsuarioPorNickname(nick);
                request.setAttribute("usuario", usuario);

                //muestraDatosRegistro(response, nombre, apellido, nick, email);
                RequestDispatcher rd = request.getRequestDispatcher("/muestraRegistrado.jsp");
                rd.forward(request, response);
                
                
            } catch (Exception exception) {

                RequestDispatcher rd = request.getRequestDispatcher("/falloAlRegistrarse.html");
                rd.forward(request, response);
            }

        } else {
            //codigo de error

            RequestDispatcher rd = request.getRequestDispatcher("/falloAlRegistrarse.html");
            rd.forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String cargaFoto(HttpServletRequest request) {

        try {

            // Create path components to save the file
            //final String path = "C:\\Users\\david\\Documents\\NetBeansProjects\\TodoMotorSport\\web\\fotosUsuarios\\";  //esta ruta hay que ponerla para que funcione siempre
            final String path = getServletContext().getRealPath("/fotosUsuarios");  //esta ruta hay que ponerla para que funcione siempre
            final Part filePart = request.getPart("imagen");
            final String fileName = getFileName(filePart);

            OutputStream out = null;
            InputStream filecontent = null;
//        final PrintWriter writer = response.getWriter();

            try {
                String rutaArchivo = path + "/" + fileName;
                System.out.println(rutaArchivo);
                File archivo = new File(rutaArchivo);
                System.out.println(archivo.getAbsolutePath());
                System.out.println(archivo.getPath());
                out = new FileOutputStream(archivo);
                filecontent = filePart.getInputStream();

                int read = 0;
                final byte[] bytes = new byte[1024];

                while ((read = filecontent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                rutaArchivo = "fotosUsuarios/" + fileName;
                return rutaArchivo;
            } catch (FileNotFoundException fne) {
                return null;
            } finally {
                if (out != null) {
                    out.close();
                }
                if (filecontent != null) {
                    filecontent.close();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(subeNoticia.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ServletException ex) {
            Logger.getLogger(subeNoticia.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getCause());
            return null;
        }

    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}
