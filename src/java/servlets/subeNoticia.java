/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import DAO.NoticiaDAO;
import DAO.NoticiaDTO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author david
 */
@MultipartConfig
@WebServlet(name = "subeNoticia", urlPatterns = {"/subeNoticia"})
public class subeNoticia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String titulo = (String) request.getParameter("titulo");
        String encabezado = (String) request.getParameter("encabezado");
        //String titulo = (String) = request.getParameter("imagen");            //esto hay que hacerlo, tienes que guardar la imagen y guardar la ruta a la imagen en la bbdd
        String cuerpo = (String) request.getParameter("cuerpo");
        String seccion = (String) request.getParameter("seccion");
        String subSeccion = "noticia";//(String) request.getParameter("subseccion");

        String cargada = cargaFoto(request);

        boolean noticiaPrincipal;
        if (request.getParameter("noticiaPrincipal").equals("Si")) {
            noticiaPrincipal = true;
        } else {
            noticiaPrincipal = false;
        }

        NoticiaDAO nDAO = new NoticiaDAO();
        NoticiaDTO noticia = new NoticiaDTO("antpesq", titulo, encabezado, cuerpo, cargada, seccion, subSeccion, noticiaPrincipal, new Date(System.currentTimeMillis()));
        nDAO.guardaNoticia(noticia);

        System.out.println(
                "Ver si son nulos req y res: "+(request!=null)+" "+
                (response!=null));
        
        RequestDispatcher rd = request.getRequestDispatcher("/index.html");
        rd.forward(request, response);
        //codigo que guarda la noticia en la bbdd
//        muestraError(response);
    }

    private void muestraError(HttpServletResponse response) {

//        RequestDispatcher rd = request.getRequestDispatcher("/login.html");
//        rd.forward(request, response);
        /**
         * Este codigo es solo para ver que si que funciona
         *
         */
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletRegistro</title>");
            out.println("</head>");
            out.println("<body><h1><center>");

            out.println("No se ha podido guardar la noticia");

            out.println("</h1></center></body>");
            out.println("</html>");
        } catch (IOException ex) {
            Logger.getLogger(subeNoticia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void muestraInfo(HttpServletResponse response, String titulo, String encabezado, String cuerpo, String seccion, String subSeccion, boolean noticiaPrincipal) {

//        RequestDispatcher rd = request.getRequestDispatcher("/login.html");
//        rd.forward(request, response);
        /**
         * Este codigo es solo para ver que si que funciona
         *
         */
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletRegistro</title>");
            out.println("</head>");
            out.println("<body>");

            out.println("titulo: " + titulo + "<br>encabezado " + encabezado + "<br>cuerpo " + cuerpo
                    + "<br>seccion " + seccion + "<br>subseccion " + subSeccion
                    + "<br>noticiaPrincipal " + noticiaPrincipal);
            //out.println("<h1>Servlet ServletRegistro at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } catch (IOException ex) {
            Logger.getLogger(subeNoticia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String cargaFoto(HttpServletRequest request) {

        try {

            // Create path components to save the file
            //final String path = "C:\\Users\\david\\Documents\\NetBeansProjects\\TodoMotorSport\\web\\uploads\\";  //esta ruta hay que ponerla para que funcione siempre
            
            final String path = getServletContext().getRealPath("/uploads");  //esta ruta hay que ponerla para que funcione siempre
            System.out.println("Ruta: "+path);
            final Part filePart = request.getPart("imagen");
            final String fileName = getFileName(filePart);

            OutputStream out = null;
            InputStream filecontent = null;

            try {
                String rutaArchivo = path +"/"+ fileName;
                System.out.println(rutaArchivo);
                File archivo = new File(rutaArchivo);
                System.out.println(archivo.getAbsolutePath()); 
                System.out.println(archivo.getPath());                
                out = new FileOutputStream(archivo);
                filecontent = filePart.getInputStream();

                int read = 0;
                final byte[] bytes = new byte[1024];

                while ((read = filecontent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                rutaArchivo = "uploads/"+ fileName;
                return rutaArchivo;
            } catch (FileNotFoundException fne) {
                return null;
            } finally {
                if (out != null) {
                    out.close();
                }
                if (filecontent != null) {
                    filecontent.close();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(subeNoticia.class.getName()).log(Level.SEVERE, null, ex);
                return null;
        } catch (ServletException ex) {
            Logger.getLogger(subeNoticia.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println(ex.getCause());
                return null;
        }
        
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

}
