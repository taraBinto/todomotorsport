/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import DAO.Pilotof1DAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author david
 */
@WebServlet(name = "subeClasificacion", urlPatterns = {"/subeClasificacion"})
public class subeClasificacion extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

//        Enumeration<String> parametros = request.getParameterNames();
        Enumeration<String> parametros = request.getParameterNames();


        Pilotof1DAO pDAO = new Pilotof1DAO();


        //codigo funcional

        String elementoActual;

        while (parametros.hasMoreElements()) {

            elementoActual = parametros.nextElement();  //nombre del objeto que estoy usando

            if (elementoActual.contains("puntos")) {    //si es una puntuacion
//                        

                String[] s = (String[]) request.getParameterValues(elementoActual);
                int puntuacion = 0;
                try {
                    puntuacion = Integer.parseInt(s[0]);
                } catch (NumberFormatException e) {
                    puntuacion = 0;
                }
                if (puntuacion != 0) {

                    String nombrePiloto = elementoActual.substring(6);
                    int puntuacionPiloto = puntuacion;
                    //actualizar la puntuacion del piloto

                    pDAO.actualizaPiloto(nombrePiloto, puntuacion);

                }
            }
            
        }

        RequestDispatcher rd = request.getRequestDispatcher("/getClasificacion");
        rd.forward(request, response);




        /**
         * Este codigo es solo para ver que si que funciona
         *
         */
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet ServletRegistro</title>");
//            out.println("</head>");
//            out.println("<body>");
//
//            String elementoActual;
//
//            while (parametros.hasMoreElements()) {
//
//                elementoActual = parametros.nextElement();  //nombre del objeto que estoy usando
//
//                if (elementoActual.contains("puntos")) {    //si es una puntuacion
////                        
//
//                    String[] s = (String[]) request.getParameterValues(elementoActual);
//                    int puntuacion = 0;
//                    try{
//                        puntuacion = Integer.parseInt(s[0]);
//                    }catch(NumberFormatException e){
//                        
//                    }
//                    if (puntuacion != 0) {
//                        
//                        String nombrePiloto = elementoActual.substring(6);
//                        int puntuacionPiloto = puntuacion;
//                        //actualizar la puntuacion del piloto
//
//                        out.println(elementoActual +" "+ s[0] + "<br>");    //linea de depuracion
//                    }
//                }
//            }
//
//            
//            out.println("</body>");
//            out.println("</html>");
//
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
