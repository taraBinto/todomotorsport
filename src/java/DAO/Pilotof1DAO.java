/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Mario
 */
public class Pilotof1DAO {

    public void actualizaPiloto(String piloto, String escuderia, int puntuacion) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            Statement statement = conexion.createStatement();
            String query = "UPDATE Pilotof1 SET escuderia='" + escuderia + "',puntuacion=" + Integer.toString(puntuacion)
                    + " WHERE piloto='" + piloto + "'";
            statement.executeUpdate(query);
            pool.freeConnection(conexion);
        } catch (Exception e) {
            pool.freeConnection(conexion);
        }
    }

    public void actualizaPiloto(String piloto, int puntuacion) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            Statement statement = conexion.createStatement();
            String query = "UPDATE Pilotof1 SET puntuacion=puntuacion+" + Integer.toString(puntuacion)
                    + " WHERE piloto like '%" + piloto + "%'";
            statement.executeUpdate(query);
            pool.freeConnection(conexion);
        } catch (Exception e) {
            pool.freeConnection(conexion);
        }
    }

    public ArrayList<Pilotof1DTO> getArrayListPilotosOrdenados() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            Statement statement = conexion.createStatement();
            String query = "SELECT * FROM Pilotof1 ORDER BY puntuacion desc";
            ResultSet resultado = statement.executeQuery(query);
            ArrayList<Pilotof1DTO> lista = new ArrayList();
            while (resultado.next()) {
                String piloto = resultado.getString("piloto");
                String escuderia = resultado.getString("escuderia");
                int puntuacion = resultado.getInt("puntuacion");
                Pilotof1DTO item = new Pilotof1DTO(piloto, escuderia, puntuacion);
                lista.add(item);
            }
            pool.freeConnection(conexion);
            return lista;
        } catch (Exception e) {
            pool.freeConnection(conexion);
            return null;
        }
    }
}
