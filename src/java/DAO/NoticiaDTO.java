/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Date;

/**
 *
 * @author david
 */
public class NoticiaDTO {

    private String titulo, encabezado, cuerpo, rutaImagen, seccion, subseccion, nickname;
    private int identificador;
    private boolean noticiaPrincipal;
    private Date fecha;

    public NoticiaDTO(String nickname, String titulo, String encabezado, String cuerpo, String rutaImagen, String seccion, String subseccion, boolean noticiaPrincipal, Date fecha) {

        this.nickname = nickname;
        this.titulo = titulo;
        this.encabezado = encabezado;
        this.cuerpo = cuerpo;
        this.rutaImagen = rutaImagen;
        this.seccion = seccion;
        this.subseccion = subseccion;
        this.noticiaPrincipal = noticiaPrincipal;
        this.fecha = fecha;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getNickName() {
        return this.nickname;
    }

    public void setNickName(String nickname) {
        this.nickname = nickname;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEncabezado() {
        return encabezado;
    }

    public void setEncabezado(String encabezado) {
        this.encabezado = encabezado;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getSubseccion() {
        return subseccion;
    }

    public void setSubseccion(String subseccion) {
        this.subseccion = subseccion;
    }

    public boolean isNoticiaPrincipal() {
        return noticiaPrincipal;
    }

    public void setNoticiaPrincipal(boolean noticiaPrincipal) {
        this.noticiaPrincipal = noticiaPrincipal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
