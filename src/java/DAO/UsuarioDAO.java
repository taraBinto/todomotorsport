/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Mario
 */
public class UsuarioDAO {

    public UsuarioDTO getUsuarioPorNickname(String nickname) {
        UsuarioDTO usuario = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            Statement statement = conexion.createStatement();
            String query = "select * from usuario where nickname='" + nickname + "'";
            ResultSet resultado = statement.executeQuery(query);
            if (resultado.next()) {
                String nombre = resultado.getString("nombre");
                String apellido = resultado.getString("apellido");
                String contrasena = resultado.getString("contrasena");
                String imagen = resultado.getString("imagen");
                String correo = resultado.getString("correo");        
                usuario = new UsuarioDTO(nickname, contrasena, nombre, apellido, correo, imagen);
            }
            pool.freeConnection(conexion);
            return usuario;
        } catch (Exception e) {
            pool.freeConnection(conexion);
            return null;
        }
    }

    public void guardaUsuario(UsuarioDTO usuario) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            Statement statement = conexion.createStatement();
            String query = "insert into usuario (nickname,nombre,apellido,contrasena,imagen,correo)"
                    + "values('" + usuario.getNickName() + "','" + usuario.getNombre() + "','" + usuario.getApellido() + "','"
                    + usuario.getContraseña() + "','" + usuario.getRutaImagen() + "','"+usuario.getCorreo()+"')";
            statement.executeUpdate(query);
            
            query = "insert into Rol (nickname,rol)"
                    + "values('" + usuario.getNickName() + "','"+"writer"+"')";

            statement.executeUpdate(query);
            
            pool.freeConnection(conexion);
        } catch (Exception e) {
            pool.freeConnection(conexion);
        }
    }

    public void guardaUsuario(String nickname, String contrasena, String nombre, String apellido, String correo, String imagen, String rol) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();

        try {
            Statement statement = conexion.createStatement();

            String query = "insert into Usuario (nickname,nombre,apellido,contrasena,imagen,correo)"
                    + "values('" + nickname + "','" + nombre + "','" + apellido + "','"
                    + contrasena + "','" + imagen + "','"+correo+"')";

            statement.executeUpdate(query);
            
            statement = conexion.createStatement();

            query = "insert into Rol (nickname,rol)"
                    + "values('" + nickname + "','"+rol+"')";

            statement.executeUpdate(query);
            
            pool.freeConnection(conexion);

        } catch (Exception e) {

            System.out.println(e.getCause());

            pool.freeConnection(conexion);
        }
    }
}
