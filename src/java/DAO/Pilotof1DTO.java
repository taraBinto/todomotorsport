/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author Mario
 */
public class Pilotof1DTO {

    String piloto, escuderia;
    int puntuacion;

    public Pilotof1DTO(String piloto, String escuderia, int puntuacion) {
        this.piloto = piloto;
        this.escuderia = escuderia;
        this.puntuacion = puntuacion;
    }

    public String getNombre() {
        return piloto;
    }

    public void setNombre(String nombre) {
        this.piloto = nombre;
    }

    public String getEscuderia() {
        return escuderia;
    }

    public void setEscuderia(String escuderia) {
        this.escuderia = escuderia;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }
}
