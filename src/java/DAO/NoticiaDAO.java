/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author david
 */
public class NoticiaDAO {

    public ArrayList<NoticiaDTO> getNoticias(String busca) {
        //De momento coge todas
        ArrayList<NoticiaDTO> catalogo = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            String query;
            
            if (busca == null) {
                //Trae las 8 ultimas noticias añadidas.
                query = "select * from noticia order by fecha desc fetch first 8 rows only";
            } else {
                //Trae las 8 ultimas noticias añadidas.
                query = "select * from noticia where titulo like '%"+busca+"%' order by fecha desc fetch first 8 rows only";
            }
            Statement statement = conexion.createStatement();
            ResultSet resultado = statement.executeQuery(query);
            catalogo = new ArrayList();
            while (resultado.next()) {
                int identificador = resultado.getInt("identificador");
                String nickname = resultado.getString("nickname");
                String titulo = resultado.getString("titulo");
                String urlimg = resultado.getString("urlimg");
                String encabezado = resultado.getString("encabezado");
                String seccion = resultado.getString("seccion");
                String subseccion = resultado.getString("subseccion");
                String esprincipal = resultado.getString("esprincipal");
                boolean principal = esprincipal.equals("T");
                String cuerpo = resultado.getString("cuerpo");
                Date fecha = resultado.getDate("FECHA");

                NoticiaDTO item = new NoticiaDTO(nickname, titulo, encabezado, cuerpo, urlimg, seccion, subseccion, principal, fecha);
                item.setIdentificador(identificador);
                catalogo.add(item);
            }
            pool.freeConnection(conexion);
            return catalogo;
        } catch (Exception e) {
            //pool.freeConnection(conexion);
            return catalogo;
        }
    }

    public NoticiaDTO getNoticiaPorIdentificador(int identificador) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            //Trae las 8 ultimas noticias añadidas.
            String query = "select * from noticia where identificador=" + Integer.toString(identificador);
            Statement statement = conexion.createStatement();
            ResultSet resultado = statement.executeQuery(query);
            NoticiaDTO noticia = null;
            if (resultado.next()) {
                String nickname = resultado.getString("nickname");
                String titulo = resultado.getString("titulo");
                String urlimg = resultado.getString("urlimg");
                String encabezado = resultado.getString("encabezado");
                String seccion = resultado.getString("seccion");
                String subseccion = resultado.getString("subseccion");
                String esprincipal = resultado.getString("esprincipal");
                boolean principal = esprincipal.equals("T");
                String cuerpo = resultado.getString("cuerpo");
                Date fecha = resultado.getDate("FECHA");

                noticia = new NoticiaDTO(nickname, titulo, encabezado, cuerpo, urlimg, seccion, subseccion, principal, fecha);
                noticia.setIdentificador(identificador);
            }
            pool.freeConnection(conexion);
            return noticia;
        } catch (Exception e) {
            pool.freeConnection(conexion);
            return null;
        }
    }

    public void guardaNoticia(NoticiaDTO noticia) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            Statement statement = conexion.createStatement();
            String consulta = "select * from Noticia order by identificador desc";
            ResultSet resultado = statement.executeQuery(consulta);
            int identificador = 0;
            if (resultado.next()) {
                identificador = resultado.getInt("identificador");
            }
            char principal;
            if (noticia.isNoticiaPrincipal()) {
                principal = 'T';
            } else {
                principal = 'F';
            }
            String query = "insert into noticia (identificador,nickname,titulo,urlimg,encabezado,seccion,subseccion,esprincipal,cuerpo,fecha)"
                    + "values(" + Integer.toString(identificador + 1) + ",'" + noticia.getNickName() + "','" + noticia.getTitulo() + "','"
                    + noticia.getRutaImagen() + "','" + noticia.getEncabezado() + "','" + noticia.getSeccion() + "','" + noticia.getSubseccion()
                    + "','" + principal + "','" + noticia.getCuerpo() + "','" + noticia.getFecha() + "')";
            statement.executeUpdate(query);
            pool.freeConnection(conexion);
        } catch (Exception e) {
            System.out.println(e.getCause());
            pool.freeConnection(conexion);
        }
    }

    public void guardaNoticia(String nickname, String titulo, String urlimg, String encabezado, String seccion, String subseccion, boolean esprincipal, String cuerpo, Date fecha) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection conexion = pool.getConnection();
        try {
            Statement statement = conexion.createStatement();
            String consulta = "select * from Noticia order by identificador desc";
            ResultSet resultado = statement.executeQuery(consulta);
            int identificador = 0;
            if (resultado.next()) {
                identificador = resultado.getInt("identificador");
            }
            char principal;
            if (esprincipal) {
                principal = 'T';
            } else {
                principal = 'F';
            }
            String query = "insert into noticia (identificador,nickname,titulo,urlimg,encabezado,seccion,subseccion,esprincipal,cuerpo,fecha)"
                    + "values(" + Integer.toString(identificador + 1) + ",'" + nickname + "','" + titulo + "','"
                    + urlimg + "','" + encabezado + "','" + seccion + "','" + subseccion + "','" + principal + "','"
                    + cuerpo + "','" + fecha + "')";
            statement.executeUpdate(query);
            pool.freeConnection(conexion);
        } catch (Exception e) {
            pool.freeConnection(conexion);
        }
    }
}
